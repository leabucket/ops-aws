#!/usr/bin/env bash

# include this in your scripts

set -e

function error_exit() {
  echo $*
  exit 1
}

function empty_bucket() {
  bundle exec ruby -e \
     "require 'aws-sdk'; \
      credentials = Aws::SharedCredentials.new(profile_name: '${AWS_PROFILE}'); \
      s3 = Aws::S3::Resource.new(credentials: credentials, region: '${AWS_REGION}'); \
      s3.bucket('$1').clear! \
     "
}

function empty_ecr() {
  bundle exec ruby -e \
     "require 'aws-sdk'; \
      credentials = Aws::SharedCredentials.new(profile_name: '${AWS_PROFILE}'); \
      ecr = Aws::ECR::Client.new(credentials: credentials, region: '${AWS_REGION}'); \
      ecr.delete_repository({ \
          force: true, \
          repository_name: '$1', \
        }) \
     "
}

# check if we have ruby
if !(which ruby >/dev/null 2>/dev/null); then
  error_exit "install ruby"
fi

# install gems if not done yet
bundle check >/dev/null || bundle install

# check if we have aws cli
if !(which aws >/dev/null 2>/dev/null); then
  error_exit "aws cli: missing, please install it from https://aws.amazon.com/de/cli/"
fi

if !(aws sts get-caller-identity >/dev/null 2>/dev/null); then
  error_exit "aws login not successful, aws configure run? swamp run?"
fi

# check if we have cli53
if !(which cli53 >/dev/null 2>/dev/null); then
  error_exit "cli53: missing, please install it from https://github.com/barnybug/cli53#installation"
fi

# check if we have jq
if !(which jq >/dev/null 2>/dev/null); then
  error_exit "jq: missing, please install, see https://stedolan.github.io/jq/download/"
fi
