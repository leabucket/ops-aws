#!/usr/bin/env bash

set -eo pipefail

SCRIPT_DIR=$(dirname "$0")
cd "${SCRIPT_DIR}"
. _aws_basis.inc.sh
cd ..
BASE_DIR=$(pwd)


AWS_PROFILE="${AWS_PROFILE:-private}"
AWS_REGION="${AWS_REGION:-eu-central-1}"

ENVIRONMENT="${ENVIRONMENT:-live}"
TEAM_NAME="${TEAM_NAME:-ops}"
SERVICE="${SERVICE:-bootstrap}"

VPC_STACK_NAME="${ENVIRONMENT}-${SERVICE}-vpc"
HOSTED_ZONE_STACK_NAME="${ENVIRONMENT}-${SERVICE}-hosted-zone"
CERTIFICATE_STACK_NAME="${ENVIRONMENT}-${SERVICE}-certificate"

## log each command before executing
trap 'echo $(date +"%Y-%m-%d %T %z") "# $BASH_COMMAND"' DEBUG

### 1. create hosted zone
bundle exec autostacker24 update \
    --profile "${AWS_PROFILE}" \
    --region "${AWS_REGION}" \
    --stack  ${HOSTED_ZONE_STACK_NAME} \
    --template cf-templates/hosted-zone.yaml \
    --param TeamName="${TEAM_NAME}" \
    --param Environment="${ENVIRONMENT}"

### 2. create certificate
bundle exec autostacker24 update \
    --profile "${AWS_PROFILE}" \
    --region "${AWS_REGION}" \
    --stack ${CERTIFICATE_STACK_NAME} \
    --template cf-templates/certificate.yaml \
    --param TeamName="${TEAM_NAME}" \
    --param Environment="${ENVIRONMENT}" \
    --param ProductName="${VPC_STACK_NAME}" \

### 3. create vpc
bundle exec autostacker24 update \
    --profile "${AWS_PROFILE}" \
    --region "${AWS_REGION}" \
    --stack ${VPC_STACK_NAME} \
    --template cf-templates/vpc.yaml \
    --param TeamName="${TEAM_NAME}" \
    --param Environment="${ENVIRONMENT}" \
    --param ProductName="${VPC_STACK_NAME}" \
    --param HostedZoneStackName="${HOSTED_ZONE_STACK_NAME}"
