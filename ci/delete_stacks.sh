#!/usr/bin/env bash

set -xeo pipefail

SCRIPT_DIR=$(dirname "$0")
cd "${SCRIPT_DIR}"
. _aws_basis.inc.sh
cd ..


AWS_PROFILE="${AWS_PROFILE:-private}"
AWS_REGION="${AWS_REGION:-eu-central-1}"

ENVIRONMENT="${ENVIRONMENT:-live}"
TEAM_NAME="${TEAM_NAME:-ops}"
SERVICE="${SERVICE:-bootstrap}"

VPC_STACK_NAME="${ENVIRONMENT}-${SERVICE}-vpc"
HOSTED_ZONE_STACK_NAME="${ENVIRONMENT}-${SERVICE}-hosted-zone"
CERTIFICATE_STACK_NAME="${ENVIRONMENT}-${SERVICE}-certificate"

# continue on errors
set +e


bundle exec autostacker24 delete \
    --profile "${AWS_PROFILE}" \
    --region "${AWS_REGION}" \
    --stack "${VPC_STACK_NAME}" \

bundle exec autostacker24 delete \
    --profile "${AWS_PROFILE}" \
    --region "${AWS_REGION}" \
    --stack "${CERTIFICATE_STACK_NAME}" \

bundle exec autostacker24 delete \
    --profile "${AWS_PROFILE}" \
    --region "${AWS_REGION}" \
    --stack "${HOSTED_ZONE_STACK_NAME}" \
