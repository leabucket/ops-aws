#!/usr/bin/env bash

set -eo pipefail

SCRIPT_DIR=$(dirname "$0")
cd "${SCRIPT_DIR}"
. _aws_basis.inc.sh
cd ..
BASE_DIR=$(pwd)


AWS_PROFILE="${AWS_PROFILE:-ops-live-admin}"
AWS_REGION="${AWS_REGION:-eu-central-1}"

ENVIRONMENT="${ENVIRONMENT:-live}"
TEAM_NAME="${TEAM_NAME:-ops}"
VERTICAL="${VERTICAL:-ops}"
SERVICE="${SERVICE:-bootstrap}"


## log each command before executing
trap 'echo $(date +"%Y-%m-%d %T %z") "# $BASH_COMMAND"' DEBUG

### 1. create hosted zone
bundle exec autostacker24 update \
    --profile "${AWS_PROFILE}" \
    --region "${AWS_REGION}" \
    --stack "${ENVIRONMENT}-${SERVICE}-dns-root" \
    --template cf-templates/dns-root.yaml \
    --param TeamName="${TEAM_NAME}"